#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

SRC_URI[sha256sum] = "45a2eb7cf72faf1ac8ea4a6dfa8891671c74f89e0265cba2737f116fbe7c22a5"
