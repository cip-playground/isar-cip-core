#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019-2024
# Copyright (c) Toshiba Corporation, 2020
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#  Nobuhiro Iwamatsu <nobuhiro1.iwamatsu@toshiba.co.jp>
#
# SPDX-License-Identifier: MIT
#

image: ghcr.io/siemens/kas/kas-isar:4.7

variables:
  GIT_STRATEGY: clone
  release: bookworm
  extension: none
  use_rt: enable
  encrypt: disable
  targz: enable
  dtb: none
  deploy: enable
  deploy_kernelci: disable
  build_swu_v2: disable
  swupdate_version: default
  test_function: swupdate
  separate_home_partition: disable

stages:
  - build
  - test
  - cve-check

default:
  before_script:
    - export http_proxy=$HTTP_PROXY
    - export https_proxy=$HTTPS_PROXY
    - export ftp_proxy=$FTP_PROXY
    - export no_proxy=$NO_PROXY
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    - export DISTRO_APT_PREMIRRORS=$DISTRO_APT_PREMIRRORS

.build_base:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH != "master"
  tags:
    - large
  variables:
    base_yaml: "kas-cip.yml:kas/board/${target}.yml"
  script:
    - if [ "${use_rt}" = "enable" ]; then base_yaml="${base_yaml}:kas/opt/rt.yml"; fi
    - if [ "${extension}" != "none" ]; then base_yaml="${base_yaml}:kas/opt/${extension}.yml"; fi
    - if [ "${targz}" = "enable" ]; then base_yaml="${base_yaml}:kas/opt/targz.yml"; fi
    - if [ "${separate_home_partition}" = "enable" ]; then base_yaml="${base_yaml}:kas/opt/separate-home-partition.yml"; fi
    - if [ "${release}" = "buster" ]; then base_yaml="${base_yaml}:kas/opt/buster.yml"; fi
    - if [ "${release}" = "bullseye" ]; then base_yaml="${base_yaml}:kas/opt/bullseye.yml"; fi
    - if [ "${release}" = "bookworm" ]; then base_yaml="${base_yaml}:kas/opt/bookworm.yml"; fi
    - if [ "${release}" = "trixie" ]; then base_yaml="${base_yaml}:kas/opt/trixie.yml"; fi
    - if [ "${encrypt}" = "enable" ]; then base_yaml="${base_yaml}:kas/opt/encrypt-data.yml"; fi
    - if [ "${watchdog}" = "disable" ]; then base_yaml="${base_yaml}:kas/opt/disable-watchdog.yml"; fi
    - if [ "${security_test}" = "enable" ]; then base_yaml="${base_yaml}:kas/opt/security_test.yml"; fi
    - if [ "${swupdate_version}" = "2022.12" ]; then base_yaml="${base_yaml}:kas/opt/swupdate-2022.12.yaml"; fi
    - echo "Building ${base_yaml}"
    - kas build ${base_yaml}
    - if [ "${deploy}" = "enable" ]; then scripts/deploy-cip-core.sh ${release} ${target} ${extension} ${dtb} ${CI_COMMIT_REF_SLUG} wic; fi
    - if [ "${build_swu_v2}" = "enable" ]; then
          mkdir build/previous-image;
          if [ "${extension}" = "security" ] || [ "${extension}" = "ebg-secure-boot-snakeoil" ]; then
              cp build/tmp/deploy/images/${target}/*.verity build/previous-image;
          else
              cp build/tmp/deploy/images/${target}/*.squashfs build/previous-image;
          fi;
          echo "PV = \"2.0\"" >> recipes-core/images/cip-core-image-security.bb;
          kas build ${base_yaml}:kas/opt/delta-update.yml;
          scripts/deploy-cip-core.sh ${release} ${target} ${extension} ${dtb} ${CI_COMMIT_REF_SLUG} swu;
      fi
    - if [ "${deploy_kernelci}" = "enable" ]; then scripts/deploy-kernelci.py ${release} ${target} ${extension} ${dtb}; fi

# base image
build:x86-uefi-base:
  extends:
    - .build_base
  variables:
    target: x86-uefi

build:bbb-base:
  extends:
    - .build_base
  variables:
    target: bbb
    dtb: am335x-boneblack.dtb

build:iwg20m-base:
  extends:
    - .build_base
  variables:
    target: iwg20m
    dtb: r8a7743-iwg20d-q7-dbcm-ca.dtb

build:hihope-rzg2m-base:
  extends:
    - .build_base
  variables:
    target: hihope-rzg2m
    dtb: r8a774a1-hihope-rzg2m-ex.dtb

build:qemu-amd64-base:
  extends:
    - .build_base
  variables:
    target: qemu-amd64
    extension: security
    use_rt: disable
    build_swu_v2: enable

build:qemu-amd64-base-kernelci:
  extends:
    - .build_base
  variables:
    target: qemu-amd64
    extension: kernelci
    use_rt: disable
    deploy: disable
    #deploy_kernelci: enable

build:qemu-arm64-base:
  extends:
    - .build_base
  variables:
    target: qemu-arm64
    extension: security
    use_rt: disable
    build_swu_v2: enable

build:qemu-arm64-base-kernelci:
  extends:
    - .build_base
  variables:
    target: qemu-arm64
    extension: kernelci
    use_rt: disable
    deploy: disable
    #deploy_kernelci: enable

build:qemu-arm-base:
  extends:
    - .build_base
  variables:
    target: qemu-arm
    extension: security
    use_rt: disable
    build_swu_v2: enable

build:qemu-arm-base-kernelci:
  extends:
    - .build_base
  variables:
    target: qemu-arm
    extension: kernelci
    use_rt: disable
    deploy: disable
    #deploy_kernelci: enable

# test
build:x86-uefi-test:
  extends:
    - .build_base
  variables:
    target: x86-uefi
    extension: test

build:bbb-test:
  extends:
    - .build_base
  variables:
    target: bbb
    extension: test
    dtb: am335x-boneblack.dtb

build:iwg20m-test:
  extends:
    - .build_base
  variables:
    target: iwg20m
    extension: test
    dtb: r8a7743-iwg20d-q7-dbcm-ca.dtb

build:hihope-rzg2m-test:
  extends:
    - .build_base
  variables:
    target: hihope-rzg2m
    extension: test
    dtb: r8a774a1-hihope-rzg2m-ex.dtb

build:qemu-amd64-test:
  extends:
    - .build_base
  variables:
    target:  qemu-amd64
    extension: test

build:qemu-arm64-test:
  extends:
    - .build_base
  variables:
    target:  qemu-arm64
    extension: test

build:qemu-arm-test:
  extends:
    - .build_base
  variables:
    target:  qemu-arm
    extension: test

build:x86-uefi-secure-boot:
  extends:
    - .build_base
  variables:
    target: x86-uefi
    extension: security
    use_rt: disable
    targz: disable
    watchdog: disable
    security_test: enable
    build_swu_v2: enable
    separate_home_partition: enable

build:qemu-amd64-swupdate:
  extends:
    - .build_base
  variables:
    target: qemu-amd64
    extension: ebg-swu
    use_rt: disable
    targz: disable
    deploy: disable

# bullseye images
build:iwg20m-bullseye:
  extends:
    - .build_base
  variables:
    target: iwg20m
    dtb: r8a7743-iwg20d-q7-dbcm-ca.dtb
    release: bullseye

build:hihope-rzg2m-bullseye:
  extends:
    - .build_base
  variables:
    target: hihope-rzg2m
    dtb: r8a774a1-hihope-rzg2m-ex.dtb
    release: bullseye

build:qemu-amd64-secure-boot-bullseye:
  extends:
    - .build_base
  variables:
    base_yaml: "kas-cip.yml:kas/board/${target}.yml"
    release: bullseye
    target: qemu-amd64
    extension: ebg-secure-boot-snakeoil
    use_rt: disable
    targz: disable
    deploy: disable
    swupdate_version: "2022.12"

# buster images
build:iwg20m-buster:
  extends:
    - .build_base
  variables:
    target: iwg20m
    dtb: r8a7743-iwg20d-q7-dbcm-ca.dtb
    release: buster

build:hihope-rzg2m-buster:
  extends:
    - .build_base
  variables:
    target: hihope-rzg2m
    dtb: r8a774a1-hihope-rzg2m-ex.dtb
    release: buster

build:qemu-amd64-secure-boot-buster:
  extends:
    - .build_base
  variables:
    base_yaml: "kas-cip.yml:kas/board/${target}.yml:kas/opt/5.10.yml"
    release: buster
    target: qemu-amd64
    extension: ebg-secure-boot-snakeoil
    use_rt: disable
    targz: disable
    deploy: disable
    encrypt: enable

# trixie images
build:qemu-riscv64:
  extends:
    - .build_base
  variables:
    target: qemu-riscv64
    release: trixie
    use_rt: disable
    targz: disable
    deploy: disable

.test-cip-core:
  stage: test
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH != "master"
  tags:
    - small
  script:
    - scripts/submit_lava.sh ${test_function} ${target} ${CI_COMMIT_SHORT_SHA} ${release} ${CI_COMMIT_REF_SLUG}
  artifacts:
    name: "$CI_JOB_NAME"
    when: always
    expire_in: 1 day
    paths:
      - results
    reports:
      junit: results/results*.xml

test:qemu-amd64-secure-boot:
  extends:
    - .test-cip-core
  needs: ["build:qemu-amd64-base"]
  variables:
    target: qemu-amd64
    test_function: secure-boot

test:qemu-arm64-secure-boot:
  extends:
    - .test-cip-core
  needs: ["build:qemu-arm64-base"]
  variables:
    target: qemu-arm64
    test_function: secure-boot

test:qemu-arm-secure-boot:
  extends:
    - .test-cip-core
  needs: ["build:qemu-arm-base"]
  variables:
    target: qemu-arm
    test_function: secure-boot

test:x86-uefi-secure-boot:
  extends:
    - .test-cip-core
  needs: ["build:x86-uefi-secure-boot"]
  variables:
    target: x86-uefi
    test_function: secure-boot

test:qemu-amd64-swupdate:
  extends:
    - .test-cip-core
  needs: ["build:qemu-amd64-base"]
  variables:
    target: qemu-amd64
    test_function: swupdate

test:qemu-arm64-swupdate:
  extends:
    - .test-cip-core
  needs: ["build:qemu-arm64-base"]
  variables:
    target: qemu-arm64
    test_function: swupdate

test:qemu-arm-swupdate:
  extends:
    - .test-cip-core
  needs: ["build:qemu-arm-base"]
  variables:
    target: qemu-arm
    test_function: swupdate

test:x86-uefi-swupdate:
  extends:
    - .test-cip-core
  needs: ["build:x86-uefi-secure-boot"]
  variables:
    target: x86-uefi
    test_function: swupdate

test:qemu-amd64-swupdate-kernel-panic:
  extends:
    - .test-cip-core
  needs: ["build:qemu-amd64-base"]
  variables:
    target: qemu-amd64
    test_function: kernel-panic

test:qemu-amd64-swupdate-initramfs-crash:
  extends:
    - .test-cip-core
  needs: ["build:qemu-amd64-base"]
  variables:
    target: qemu-amd64
    test_function: initramfs-crash

test:qemu-amd64-IEC:
  extends:
   - .test-cip-core
  needs: ["build:qemu-amd64-base"]
  variables:
    target: qemu-amd64
    test_function: IEC

test:qemu-arm64-IEC:
  extends:
   - .test-cip-core
  needs: ["build:qemu-arm64-base"]
  variables:
    target: qemu-arm64
    test_function: IEC

test:qemu-arm-IEC:
  extends:
   - .test-cip-core
  needs: ["build:qemu-arm-base"]
  variables:
    target: qemu-arm
    test_function: IEC

test:x86-uefi-IEC:
  extends:
   - .test-cip-core
  needs: ["build:x86-uefi-secure-boot"]
  variables:
    target: x86-uefi
    test_function: IEC

cve-checks:
  stage: cve-check
  needs: []
  image: registry.gitlab.com/cip-project/cip-core/debian-cve-checker:build-latest
  tags:
    - large
  script:
    - scripts/run-cve-checks.sh
  when: manual
  allow_failure: true
  artifacts:
    expire_in: 1 day
    paths:
      - cve-reports

include: '.reproducible-check-ci.yml'
